import java.util.Arrays;

public class Anagram {
    static void checkAnagram(String str1, String str2) {

        String string1 = str1;
        String string2 = str2;
        boolean anagram = true;
        if (string1.length() != string2.length()) {
            anagram = false;
        } else {
            char[] ArrayS1 = string1.toLowerCase().toCharArray();
            char[] ArrayS2 = string2.toLowerCase().toCharArray();
            Arrays.sort(ArrayS1);
            Arrays.sort(ArrayS2);
            anagram = Arrays.equals(ArrayS1, ArrayS2);
        }
        if (anagram) {
            System.out.println(string1 + " and " + string2 + " are anagrams");
        } else {
            System.out.println(string1 + " and " + string2 + " are not anagrams");
        }
    }

    public static void main(String[] args) {
        checkAnagram("bleat", "table");
        checkAnagram("eat", "tar");
    }
}  